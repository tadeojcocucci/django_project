from django.contrib import admin
from django.urls import path
from . import views

app_name = 'authentication'
urlpatterns = [
    path('login/', views.login_view, name='login'),
    path('logout/', views.logout_view, name='logout'),
    path('signup/', views.signup_view, name='signup'),
    path('<int:user_id>/', views.profile_view, name='profile'),
]
