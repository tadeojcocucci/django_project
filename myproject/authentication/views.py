from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from .models import User
from django.urls import reverse

def login_view(request):
    if request.user.is_authenticated:
        return render(request, 'redirect.html',
         {
         'link': reverse('authentication:profile', args=(request.user.id,)),
         'text': 'Ya estas logueado, ir tu perfil:'})

    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            return HttpResponseRedirect(reverse('authentication:profile', args=(user.id,)))
        else:
            return render(request, 'authentication/login.html', {'error_msg': 'datos incorrectos'})
    else:
        return render(request, 'authentication/login.html')

def logout_view(request):
    logout(request)
    return HttpResponseRedirect(reverse('authentication:login'))

def signup_view(request):
    if request.user.is_authenticated:
        return render(request, 'redirect.html',
         {
         'link': reverse('authentication:profile', args=(request.user.id,)),
         'text': 'Ya estas logueado, ir tu perfil:'})

    if request.method == 'POST':
        username = request.POST['username']
        email = request.POST['email']
        password = request.POST['password']
        try:
            u = User.objects.create_user(username=username, email=email, password=password)
            u.save()
        except:
            return render(request, 'authentication/signup.html',
             {'error_msg': 'No es posible crear cuenta con esos datos'})
        return HttpResponseRedirect(reverse('authentication:login'))
    else:
        return render(request, 'authentication/signup.html', {})

@login_required(login_url='authentication:login')
def profile_view(request, user_id):
    if user_id !=request.user.id:
        return HttpResponse('Acceso denegado!')

    u = User.objects.get(id=user_id)
    name = u.username
    return render(request, 'authentication/profile.html', {'username': name})
